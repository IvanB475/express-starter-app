const express = require('express');
const morgan = require('morgan');
const md5 = require('md5');

const concateStudentData = require('./utils/concateStudentData');
const authHeaders = require('./middleware/auth');

const app = express();
const PORT = process.env.PORT || 8002;


//registering necessary middlewares
app.use(express.json());
app.use(morgan('tiny'));


// Registering get route on path '/', using write and end methods
app.get('/', (req, res) => {
    res.write("<h1>My First Express App - Author: Ivan Beljan</h1>");
    res.end();
});


// Registering get route on path '/not-found', using status and send methods
app.get('/not-found', (req, res) => {
    res.status(404).send("<h3>Sorry. Page is not found</h3>");
});

// Registering get route on path '/error', throws new error and passes it to error handling middleware
app.get('/error', async (req, res, next) => {
    try {
        throw new Error('Just for error testing');
    } catch(e) {
        next(e);
    }
});

//Register a get route that takes in 3 query params, passes these params to concateData function and passes result as response
app.get('/student-data', async (req, res, next) => {
    try { 
    const { firstName, lastName, email } = req.query;
    const concatedStudentData = concateStudentData.concateData(firstName, lastName, email);
    res.status(200).json(concatedStudentData);
    } catch(e) {
        next(e)
    }
});


//Registering post route on '/authorization' path that takes in 4 body params, contactes them with underline, hashes the result and passes it to authorization header and json response
app.post('/authorization', async(req, res, next) => {
    try { 
        const { firstName, lastName, id, email } = req.body;
        const concatedPersonalData = concateStudentData.concateData(firstName, lastName, email);
        const concatedpersonalDataWithId = id + '_' + concatedPersonalData;
        const hashedPersonalData = md5(concatedpersonalDataWithId);
        res.setHeader('Authorization', hashedPersonalData);
        res.status(201).json(hashedPersonalData);
    } catch(e) {
        next(e);
    }
})

// Registering a get route on '/private-route' path that checks if customAuthHeader exists, and responds accordingly
app.get('/private-route', authHeaders.validateAuthHeader, authHeaders.validateUser, (req, res) => {
        req.customAuthHeader ? res.status(200).json({authHeaderExist: true, value: req.customAuthHeader}) : res.status(200).json({authHeaderExist: false, value: ""}) 
})


//Simple error handling middleware that checks if status code exists and is different than 500, if it is prints existing status code, otherwise prints 500 
app.use(function (err, req, res, next) {
    err.status && err.status !== 500 ? res.status(err.status).json(err.message) : res.status(500).send(`<h1>${err.message}</h1>`);
});



app.listen(PORT);
